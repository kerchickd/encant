/***************************************************************
 * Name:      EncAntApp.h
 * Purpose:   Defines Application Class
 * Author:    hhh ()
 * Created:   2019-03-02
 * Copyright: hhh ()
 * License:
 **************************************************************/

#ifndef ENCANTAPP_H
#define ENCANTAPP_H

#include <wx/app.h>

class EncAntApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // ENCANTAPP_H
